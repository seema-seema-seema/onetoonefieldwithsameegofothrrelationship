from django.shortcuts import render
from .models import Category, Product

# Create your views here.


def index(request):
    cat = Category.objects.all()
    prod = Product.objects.all()
    context = {
        'categories': cat,
        'products': prod,
    }
    return render(request, "index.html", context)
